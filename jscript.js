




//Dropdownlist-toggle
$('.drop-down [data-toggle]').click(function(){
    $( this ).closest('.drop-down').toggleClass('open');
});
//Dropdownlist-select
$('.drop-down ul li').click(function(){
    //Get Values of clicked list item
    let val = $( this ).attr('value');
    let content = $( this ).html();

    //Apply Values to titel and input box
    let dropdown = $( this ).closest('.drop-down');
    dropdown.find('[data-toggle]').html(content);
    dropdown.find('input').attr('value', val);

    //Close Select box
    $( this ).closest('.drop-down').toggleClass('open');

    //Set selected-property for styling
    $( this ).siblings('li').removeAttr('selected');
    $( this ).attr('selected', true);
});

//Btn close-open
$('.btn-buy').click(function (){
    $('.buy-form').slideToggle();
    $('.btn-open').slideToggle();
    $('.btn-close').slideToggle();
    $('.btn-buy').toggleClass('btn-buy-close');
});
